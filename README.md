Passion Project to go along with the [DLN Community](discourse.destinationlinux.network).

The linux file system was a topic for several weeks and this is a compilation of those discussions.

This project can be contributed to by anyone in the community. It would go nicely with the spotlight on git as people can contribute using git as a platform.

The linux file system tip of the week/spotlight started at [Episode 184](https://destinationlinux.org/episode-184/)

All shows can be referenced [here](https://destinationlinux.org/archive/)

Order is:
- [x] /temp (184)
- [x] /bin (185)
- [x] /boot (186)
- [x] /dev (187)
- [x] /etc (188)
- [x] /lib (189)
- [x] /media (190)
- [x] /opt (191)
- [x] /proc (192)
- [x] /root (193)
- [x] /run (194)
- [x] /sbin (195)
- [x] /usr (196)
- [x] /srv (197)
- [x] /sys (198)
- [x] /var (199)

The package `poc-streamer` contains the tool [mp3cut](http://manpages.ubuntu.com/manpages/hirsute/en/man1/mp3cut.1.html) which is really nice and fast for editing the shows based on Michael's time stamps. I edited the audio from 186 using the following command:

```mp3cut -o new_186.mp3 -t 45:18+500-47:18+250 186.mp3```

~~It would be great if we could use an ffmpeg script or something similar to trim the sections based on the times posted by Michael.~~
